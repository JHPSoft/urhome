import React from 'react';
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";

import Sidebar from '../component/My/Sidebar';
import Contents from '../component/My/Contents';

import myStore from "../store/My/store";

// import "@public/css/views/my.css";
// import "../css/views/my.css";

const My = () => {
    return (
        <Provider store={myStore}>
            <Helmet>
                <link rel="stylesheet" type="text/css" href="https://jhpsoft.gitlab.io/urhome/css/views/my.css" />
                <link rel="stylesheet" type="text/css" href="../css/views/my.css" />
            </Helmet>
            <Sidebar />
            <Contents />
        </Provider>
    );
}

export default My;

import React, { useState, useEffect } from'react';
import { Link } from 'react-router-dom';

const Main = () => {
    return (
        <div className="App">
            <header>
                <img src="https://dummyimage.com/256x64/7f7f7f/ffffff.jpg?text=urHome Logo Place" className="App-logo" alt="logo" />
            </header>
            <main>
                <Link to="/my"><button type="button" className="success">나만의 페이지 만들기</button></Link>
            </main>
        </div>
    );
}

export default Main;
import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addWidget } from "../../actions/My/widgetActions";
import { toggleMenuCollapse } from "../../actions/My/menuActions";
import { hideSidebar, showSidebar } from "../../actions/My/sidebarActions";

const apiURL = process.env.REACT_APP_URL;

const Sidebar = () => {
    const dispatch = useDispatch();
    const handleWidgetClick = (url, width, height, resizeable) => {
        const id = Date.now().toString();
        dispatch(addWidget(id, url, width, height, resizeable));
        dispatch(hideSidebar());
        dispatch(toggleMenuCollapse(false))
    };
    
    return (
        <div id="widgetList">
            <div className="items" onClick={() => handleWidgetClick(apiURL + "/widgets/digitalclock", 550, 130, true) } data-url="widgets/digitalclock/index.html" data-width="550" data-height="130" data-resizable="t" data-category="clock">
                <img src="https://dummyimage.com/64x64/7f7f7f/ffffff.jpg" />
                <h3>디지털시계</h3>
                <p>디지털시계</p>
                <small>[- license -]</small>
            </div>
        </div>
    );
};

export default Sidebar;
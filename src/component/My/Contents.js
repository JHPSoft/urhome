import React from "react";

import Background from "./Background";
import BackgroundContextMenu from "./BackgroundContextMenu";
import Sheet from "./Sheet";
import Menu from "./Menu";
import ResizeHandler from "./ResizeHandler";

const apiURL = process.env.REACT_APP_URL;

const Contents = () => {
    return (
        <div id="contents">
            <label className="dpn">{apiURL}</label>
            <Background />
            <BackgroundContextMenu />
            <Sheet />
            <Menu />
            <ResizeHandler />
        </div>
    );
};

export default Contents;
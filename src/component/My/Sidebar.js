import React, { useEffect } from "react";
import { useSelector } from "react-redux";

import SidebarMenu from "./SidebarMenu";
import WidgetList from "./WidgetList";

const Sidebar = () => {
    const isSidebarCollapsed = useSelector(state => state.sidebar.isSidebarCollapse);

    return (
        <div id="sidebar" className={isSidebarCollapsed ? "" : "show"}>
            <SidebarMenu />
            <WidgetList />
        </div>
    );
};

export default Sidebar
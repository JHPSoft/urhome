import React from "react";

const ResizeHandler = () => {
    return (
        <div id="resize-handler">
            <span className="glyphicon glyphicon-remove" id="remove-icon">
            </span>
        </div>
    );
};

export default ResizeHandler;
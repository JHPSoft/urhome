import React, { useCallback, useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { addWidget } from "../../actions/My/widgetActions";
import { toggleMenuVisibility, toggleMenuMode, toggleMenuCollapse } from "../../actions/My/menuActions";
import { toggleCollapseSideBar, hideSidebar, showSidebar } from "../../actions/My/sidebarActions";


const Sheet = () => {
    const dispatch = useDispatch();
    const widgets = useSelector((state) => state.widget.list);
    const HEIGHT = window.innerHeight;
    const WIDTH = window.innerWidth;

    const [posDiff, setPosDiff] = useState({ x: 0, y: 0 });

    const [dragging, setDragging] = useState(false);

    const [draggingID, setDraggingID] = useState(-1);
    const [prevZIndex, setPrevZIndex] = useState(-1);

    /**
     * 시트 클릭시 상단메뉴 보이게, 사이드바는 안보이게
     */
    const handleClick = () => {
        dispatch(hideSidebar());
        showMenu();
    }

    /**
     * 상단메뉴 안보이게
     */
    const hideMenu = () => {
        dispatch(toggleMenuCollapse(true));
    }
    
    /**
     * 상단메뉴 보이게
     */
    const showMenu = () => {
        dispatch(toggleMenuCollapse(false));
    }

    const handleMouseDown = (e) => {
        setDragging(true);
        hideMenu();
        const rect = e.target.getBoundingClientRect();
        setPosDiff({
            x: rect.x - e.clientX,
            y: rect.y - e.clientY
        })
        setDraggingID(e.target.id);
        setPrevZIndex(e.target.style.zIndex);

        if(e.target.id !== "sheet")
            e.target.style.zIndex = 10000;
    };

    const handleMouseMove = (e) => {
        if (dragging && draggingID === e.target.id) {
            e.target.style.top = e.clientY + posDiff.y;
            e.target.style.left = e.clientX + posDiff.x;
        }
    };

    const handleMouseUp = (e) => {
        setDragging(false);
        showMenu();
        setPosDiff({
            x: 0,
            y: 0,
        });
        if(e.target.id !== "sheet")
            e.target.style.zIndex = prevZIndex;
    };

    
    return (
        <div id="sheet" onMouseMove={handleMouseMove} onClick={handleClick}> 
            {widgets.map((widget) => {
                return (
                    <div
                        key={widget.id}
                        id={widget.id}
                        className="widgets"
                        style={{
                            width: widget.width,
                            height: widget.height,
                            left: (WIDTH - widget.width) / 2,
                            top: (HEIGHT - widget.height) / 2,
                            position: "absolute",
                            zIndex: 5000,
                        }}
                        onMouseDown={handleMouseDown}
                        onMouseUp={handleMouseUp}
                    >
                        <iframe
                            src={widget.url}
                            width={widget.width}
                            height={widget.height}
                            frameBorder="0"
                            allowFullScreen={widget.resizeable}
                            style={{
                                "pointerEvents": "none"
                            }}
                        ></iframe>
                    </div>
                )
            })}
        </div>
    );
};


export default Sheet;
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { hideSidebar } from "../../actions/My/sidebarActions";


const Background = () => {
    const dispatch = useDispatch();

    const handleClick = () => {
        dispatch(hideSidebar());
    };

    return (
        <div id="background-images" onClick={handleClick}>
        </div>
    );
};

export default Background;
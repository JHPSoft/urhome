import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleCollapseSideBar, hideSidebar, showSidebar, testIncreaseCount } from "../../actions/My/sidebarActions";
import { toggleMenuVisibility, toggleMenuMode, toggleMenuCollapse } from "../../actions/My/menuActions";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faSave, faScrewdriverWrench } from "@fortawesome/free-solid-svg-icons";

const Menu = () => {
    const dispatch = useDispatch();

    const handleClick = useCallback(() => {
        dispatch(toggleMenuCollapse(true));
        dispatch(showSidebar());
    }, [dispatch]);

    const handleSaveClick = useCallback(() => {
        dispatch(toggleMenuMode());
    }, [dispatch]);
    
    const changeMenuVisibility = useCallback((value) => {
        dispatch(toggleMenuVisibility(value));
    }, [dispatch]);


    /**
     * 메뉴 표시여부
     */
    const isMenuCollapsed = useSelector(state => state.menu.isMenuCollapse);
    /**
     * 메뉴(⨁,🔧) 표시여부
     */
    const isMenuVisible = useSelector(state => state.menu.isMenuVisible);
    const isMenuEditMode = useSelector(state => state.menu.isMenuEditMode);

    return (
        <>
            <div id="menu" onMouseEnter={() => changeMenuVisibility(true)} onMouseLeave={() => changeMenuVisibility(false)} 
                className={`${isMenuVisible ? "in" : "out"} ${isMenuCollapsed ? "hide" : "show"} ${isMenuEditMode ? "edit" : "save"}`}>
                <div>
                    <button id="add" type="button" className="btn btn-default btn-xl" onClick={handleClick}>
                        <FontAwesomeIcon icon={faPlus} />
                    </button>
                    <button id="save" type="button" className="btn btn-primary btn-xl" onClick={handleSaveClick}>
                        <FontAwesomeIcon icon={isMenuEditMode ? faSave : faScrewdriverWrench} />
                    </button>
                </div>
            </div>
        </>
    );
};

export default Menu;
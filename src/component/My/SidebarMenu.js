import React from "react";

const SidebarMenu = () => {
    return (
        <div id="sidebar-menu">
            <h2 className="text-center">WIDGETS</h2>
            <div className="input-group">
                <div className="input-group-btn">
                    <button type="button" id="dropdown-category" className="btn btn-default dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">전체</button>
                    <ul className="dropdown-menu" role="menu">
                        <li className="dropdown-item">전체</li>
                        <li className="divider"></li>
                        <li className="dropdown-item">유틸리티</li>
                        <li className="dropdown-item">뉴스 및 날씨</li>
                        <li className="dropdown-item">시간</li>
                        <li className="dropdown-item">검색도구</li>
                        <li className="dropdown-item">스포츠</li>
                    </ul>
                </div>
                <input type="text" className="form-control" aria-label="..." id="search" />
            </div>
        </div>
    );
};

export default SidebarMenu;
import { configureStore } from "@reduxjs/toolkit";
import sidebarReducer from "../../reducers/My/sidebarReducer";
import menuReducer from "../../reducers/My/menuReducer";
import widgetReducer from "../../reducers/My/widgetReducer";

const store = configureStore({
    reducer: {
        sidebar: sidebarReducer,
        menu: menuReducer,
        widget: widgetReducer
    },
    preloadedState: {
        sidebar: {
            isSidebarCollapse: true,
            testCount: 0,
        },
        menu: {
            isMenuVisible: true,
            isMenuEditMode: true,
            isMenuCollapse: false,
        },
        widget: {
            list: [],
        }
    }
});

export default store;
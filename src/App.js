import React, { useState, useEffect } from 'react';
import { Routes, Route } from "react-router-dom";
import Main from "./views/Main";
import My from "./views/My";
import NotFound from './views/NotFound';

const App = () => {
    return (
        <Routes>
            <Route path="/" element={<Main />} />
            <Route path="/my" element={<My />} />
            <Route path="/*" element={<NotFound />} />
        </Routes>
    );
};

export default App;
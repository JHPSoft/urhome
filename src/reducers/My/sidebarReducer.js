import { TOGGLE_COLLAPSE_SIDEBAR, TEST_INCREASE_COUNT, HIDE_SIDEBAR, SHOW_SIDEBAR } from '../../actions/My/types';

const initialState = {
    isSidebarCollapse: true,
    testCount: 0
};

const sidebarReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_COLLAPSE_SIDEBAR:
            return {
                ...state,
                isSidebarCollapse: !state.isSidebarCollapse
            };
        case HIDE_SIDEBAR:
            return {
                ...state,
                isSidebarCollapse: true
            };
        case SHOW_SIDEBAR:
            return {
                ...state,
                isSidebarCollapse: false
            };
        case TEST_INCREASE_COUNT:
            return {
                ...state,
                testCount: state.testCount + 1 // testCount 값을 1 증가시킴
            };
        default:
            return state;
    }
};

export default sidebarReducer;
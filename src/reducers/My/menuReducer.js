import { TOGGLE_MENU_VISIBILITY, TOGGLE_MENU_MODE, TOGGLE_MENU_COLLAPSE } from '../../actions/My/types';

const initialState = {
    isMenuVisible: true,
    isMenuEditMode: true,
    isMenuCollapse: false,
};

const menuReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MENU_VISIBILITY:
            return {
                ...state,
                isMenuVisible: action.payload !== undefined ? action.payload : !state.isMenuVisible,
            };
        case TOGGLE_MENU_MODE:
            return {
                ...state,
                isMenuEditMode: !state.isMenuEditMode
            };
        case TOGGLE_MENU_COLLAPSE:
            return {
                ...state,
                isMenuCollapse: action.payload !== undefined ? action.payload : !state.isMenuCollapse
            };
        default:
            return state;
    }
};

export default menuReducer;
import { ADD_WIDGET } from '../../actions/My/types';

const initialState = {
    list: [],
};

const widgetReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_WIDGET:
            return {
                ...state,
                list: [
                    ...state.list,
                    {
                        id: action.payload.id,
                        url: action.payload.url,
                        width: action.payload.width,
                        height: action.payload.height,
                        resizeable: action.payload.resizeable,
                    },
                ],
            };
        default:
            return state;
    }
};

export default widgetReducer;
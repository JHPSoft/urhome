import { TOGGLE_MENU_VISIBILITY, TOGGLE_MENU_MODE, TOGGLE_MENU_COLLAPSE } from './types';

export const toggleMenuVisibility = (payload) => {
    return {
        type: TOGGLE_MENU_VISIBILITY,
        payload
    };
};

export const toggleMenuMode = () => {
    return {
        type: TOGGLE_MENU_MODE
    };
};

export const toggleMenuCollapse = (payload) => {
    return {
        type: TOGGLE_MENU_COLLAPSE,
        payload
    };
};
import { TOGGLE_COLLAPSE_SIDEBAR, TEST_INCREASE_COUNT, HIDE_SIDEBAR, SHOW_SIDEBAR } from './types';

export const toggleCollapseSideBar = () => {
    return {
        type: TOGGLE_COLLAPSE_SIDEBAR
    };
};

export const hideSidebar = () => {
    return {
        type: HIDE_SIDEBAR
    };
};

export const showSidebar = () => {
    return {
        type: SHOW_SIDEBAR
    };
};

export const testIncreaseCount = () => {
    return {
        type: TEST_INCREASE_COUNT
    };
};
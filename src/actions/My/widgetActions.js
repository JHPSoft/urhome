import { ADD_WIDGET } from './types';

export const addWidget = (id, url, width, height, resizeable) => ({
    type: ADD_WIDGET,
    payload: {
        id,
        url,
        width,
        height,
        resizeable,
    },
});